#include <stdio.h>
#include <time.h>

int display_time() {
  time_t     now;
  struct tm  current_time;
  char       buf[80];

  // Get current time
  time(&now);

  // Format time, "ddd yyyy-mm-dd hh:mm:ss zzz"
  current_time = *localtime(&now);
  strftime(buf, sizeof(buf), "%a %Y-%m-%d %H:%M:%S %Z", &current_time);
  printf("%s\n", buf);
  return 0;
}

int display_time_from_epoch(time_t rawtime) {
    struct tm  ts;
    char       buf[80];

    // Format time, "ddd yyyy-mm-dd hh:mm:ss zzz"
    ts = *localtime(&rawtime);
    strftime(buf, sizeof(buf), "%a %Y-%m-%d %H:%M:%S %Z", &ts);
    printf("%s\n", buf);
    return 0;
}


int main() {

  time_t current_time;
  char* current_time_s;

  display_time();
  
  current_time = time(NULL);
  printf("Epoch Time : %ld\n", current_time);

  current_time_s = ctime(&current_time);
  printf("Human readable (via ctime) : %s", current_time_s);

  printf("Time of 1602427118\n");
  display_time_from_epoch(1602427118);
  
  return 0;
}
