# Interoperability Report

* Author : Alexandre Verdet
* Last update : 09/12/20
* [Course Link](https://ci.mines-stetienne.fr/i2si/interop/)

# Evaluation Table

### [Program date and time manipulation and conversion](0_datetime/)


### [Base64 convert/encode](1_base64/encode_decode_text_terminal.png)


### [Base64 image](1_base64/image/)


### [Program Base64](1_base64/code/)


### [Screenshots load/export CSV](2_tabular_data/screenshots/)


### [Program with some CSV library](2_tabular_data/tabular.py)


### [JSON today’s weather forecast](3_json/current_weather.json)


### [JSON Fayol sensor](3_json/sensor.json)


### [JSON online syntax/valid/prettify](3_json/json_online_beautifier.png)


### [JSON text editor syntax/valid/prettify](3_json/text_editor/)


### [XML musicbrainz](4_xml/musicbrainz.xml)


### [XML agenda](4_xml/agenda.xml)


### [XML online syntax/valid/prettify](4_xml/xml_online_beautifier.png)


### [XML text editor syntax/valid/prettify](4_xml/text_editor/)


### [YAML API](5_yaml/fayolapi.yaml)


### [YAML online syntax/valid/prettify](5_yaml/online/)


### [YAML text editor syntax/valid/prettify](5_yaml/emacs_yaml/highlight.png)


### [JSON-LD storey](6_jsonld/storey2.jsonld)


### [JSON-LD room](6_jsonld/room221.jsonld)


### [Knowledge graph generated using the JSON-LD playground](6_jsonld/jsonld_graph.png)


### [JSONSchema extended FIWARE meteo](7_jsonSchema/schema_weather_forecast.json)


### [JSONSchema in the API specification](5_yaml/fayolapi.yaml)


### [JSONPath query online tool](8_jsonPath/json_path_online_query.png)


### [JSONPath four queries](8_jsonPath/)


### [XPath query online tool](9_xPath/xpath_online_query.png)


### [XPath three queries](9_xPath/agenda_queries.txt)


### [JSONSchema -> OOP Classes](10_oop/jsonschema2pojo/)


### [JSON <-> OOP Objects](10_oop/json-tests/)
