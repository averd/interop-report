# Queries to ask with current_weather.json

What is the current temperature\
`$.weather.current.temperature`

What is the forecasted weather at 4pm\
`$.weather.dayForecast[?(@.day_off==0)].hourForecast[?(@.hour==16)]`

What will be the humidity at 4pm\
`$.weather.dayForecast[?(@.day_off==0)].hourForecast[?(@.hour==16)].humidity`

What will be the forecasted weather type (sunny, overcast cloud, ..?) when the humidity is above 65%\
`$.weather.dayForecast.*.hourForecast[?(@.humidity>65)].weatherType`


![](json_path_online_query.png)

![](json_path_weather_query.png)
