# Emacs as XML Editor

## Invalid XML file

![](xml_emacs_invalid.png)

## Valid XML file

![](xml_emacs_valid.png)
