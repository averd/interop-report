package jsonTests;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.IOException;

public class TestJsonExport {
	
	public static void jsonExport(Personne pers, String file) {

		final GsonBuilder builder = new GsonBuilder();
		final Gson gson = builder.create();

		final String json = gson.toJson(pers);
		System.out.println("Resultat = " + json);

		try {
			FileWriter myWriter = new FileWriter(file);
			myWriter.write(json);
			myWriter.close();
			System.out.println(file + " Json file successfully created.");
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
	}

	public static void main(final String[] args) {

		Personne pers = new Personne();
		pers.setNom("Verdet");
		pers.setPrenom("Alexandre");
		pers.setTaille(185);
		pers.setAdulte(true);

		if (args.length > 0) {
			jsonExport(pers, args[0]);
		}
		else {
			jsonExport(pers, "exported.json");
		}
		
	}
}
