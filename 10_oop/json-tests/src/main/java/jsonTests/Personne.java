package jsonTests;

public class Personne {
	private String nom;
	private String prenom;
	private int taille;
	private boolean adulte;

	public String getNom() {
		return nom;
	}

	public void setNom(String newNom) {
		this.nom = newNom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String newPrenom) {
		this.prenom = newPrenom;
	}

	public int getTaille() {
		return taille;
	}

	public void setTaille(int newTaille) {
		this.taille = newTaille;
	}

	public boolean getAdulte() {
		return adulte;
	}

	public void setAdulte(boolean newAdulte) {
		this.adulte = newAdulte;
	}

	public void print() {
		System.out.println(this.nom);
		System.out.println(this.prenom);
		System.out.println(this.taille);
		System.out.println(this.adulte);
	}

	public boolean equals(Personne b) {
		return this.nom.equals(b.nom) & this.prenom.equals(b.prenom) & this.taille == b.taille
				& this.adulte == b.adulte;
	}

}
