package jsonTests;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TestJsonImport {

	public static Personne jsonImport(final String file) {

		final Gson gson = new GsonBuilder().create();

		// final String json =
		// "{\"nom\":\"Verdet\",\"prenom\":\"Francois\",\"taille\":\"180\",\"adulte\":true}";

		try {
			File myObj = new File(file);
			Scanner myReader = new Scanner(myObj);
			String json = myReader.useDelimiter("\\A").next();
			myReader.close();
			final Personne personne = gson.fromJson(json, Personne.class);

			return personne;
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
			return null;
		}

	}
	
	public static void main(final String[] args) {
		Personne personne = new Personne();
		if (args.length > 0) {
			personne = jsonImport(args[0]);
		}
		else {
			personne = jsonImport("import.json");
		}
		System.out.println("Imported instance from Json file:");
		personne.print();
	}
}
