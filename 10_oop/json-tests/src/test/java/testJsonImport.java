import static org.junit.Assert.*;

import org.junit.Test;

import jsonTests.Personne;
import jsonTests.TestJsonImport;

public class testJsonImport {

	@Test
	public void jtestJsonImport() {
		Personne expected = new Personne();
		expected.setNom("Verdet");
		expected.setPrenom("Chloe");
		expected.setTaille(165);
		expected.setAdulte(false);
		
		Personne result = TestJsonImport.jsonImport("import.json");
		
		assertTrue(result.equals(expected));
	}

}
