import static org.junit.Assert.*;

import org.junit.Test;

import jsonTests.Personne;
import jsonTests.TestJsonExport;
import jsonTests.TestJsonImport;

public class testJsonExport {

	@Test
	public void jtestJsonExport() {
		Personne pers = new Personne();
		pers.setNom("Verdet");
		pers.setPrenom("Alexandre");
		pers.setAdulte(true);
		pers.setTaille(185);
		
		TestJsonExport.jsonExport(pers, "target/junit_exported.json");
		Personne result = TestJsonImport.jsonImport("target/junit_exported.json");
		assertTrue(result.equals(pers));
	}

}
