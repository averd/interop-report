# Json and OOP manipulation

### Import from *import.json* (which is also default)
`mvn clean compile exec:java -Dexec.mainClass="jsonTests.TestJsonImport" -Dexec.args="import.json"`

### Export in *exported_file.json*, default: *exported.json*
`mvn clean compile exec:java -Dexec.mainClass="jsonTests.TestJsonExport" -Dexec.args="exported_file.json"`
