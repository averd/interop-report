# JSONSchema to OOP Classes

```xml
*pom.xml*
<plugin>
	<groupId>org.jsonschema2pojo</groupId>
	<artifactId>jsonschema2pojo-maven-plugin</artifactId>
	<version>1.0.2</version>
	<configuration>
		<sourceDirectory>${basedir}/src/main/resources/schema</sourceDirectory>
		<targetPackage>com.example.types</targetPackage>
	</configuration>
	<executions>
		<execution>
			<goals>
				<goal>generate</goal>
			</goals>
		</execution>
	</executions>
</plugin>
```

To generate OOP classes
`mvn generate-sources`
