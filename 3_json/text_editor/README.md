# Emacs as Json Editor

## Validation

![](emacs_json_non_valid.png)

## Beautifier Before

![](emacs_json_beautifier_before.png)

## Beautifier After

![](emacs_json_beautifier_after.png)
