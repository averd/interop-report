import sys
import base64


def main():
    if len(sys.argv) > 1:
        print("Initial sentence: "+sys.argv[1]+"\n")
        initial_bytes = sys.argv[1].encode("ascii")
        encoded_sentence = base64.b64encode(initial_bytes)
        print("Encoded sentence: "+str(encoded_sentence)+"\n")
        return 0

    print("Please enter a sentence to convert to base64\n")
    return 1

if __name__ == "__main__":
    main()
    
