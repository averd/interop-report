import sys
import base64


def main():
    if len(sys.argv) > 1:
        print("Encoded sentence: "+str(sys.argv[1])+"\n")
        initial_bytes = sys.argv[1].encode("ascii")
        decoded_sentence = base64.b64decode(initial_bytes)
        sentence = decoded_sentence.decode("ascii")
        print("Decoded sentence: "+str(sentence)+"\n")
        return 0

    print("Please enter a base64 encoded text\n")
    return 1

if __name__ == "__main__":
    main()
    
